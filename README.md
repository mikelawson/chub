# chub

## Huh?

Ok, if you've seen Chubbyemu's videos on youtube, you'll know how his titles
make everything seem a little farfetched. So, I got the idea to do this silly little
thing and just have a title generator. Enjoy!

If you haven't, his videos are awesome, go check them out!!

[Chubbyemu](https://www.youtube.com/channel/UCKOvOaJv4GK-oDqx-sj7VVg/featured)

Requests? Hit me up [mlawson1986@gmail.com](mailto:mlawson1986@gmail.com)
